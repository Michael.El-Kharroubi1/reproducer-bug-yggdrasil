#!/bin/sh
#SBATCH --job-name=reproducer
#SBATCH --time=00:01:00
#SBATCH --partition=shared-gpu
#SBATCH --gpus-per-task=1
#SBATCH --ntasks=4
#SBATCH --output=reproducer-%j.out
#SBATCH --constraint="COMPUTE_TYPE_TURING|"

ml GCC/12.3.0 CUDA/12.3.0 OpenMPI/4.1.5

srun ./reproducer