# Reproducer bug Yggdrasil

## Loaded Modules

<pre>(<font color="#06989A">yggdrasil</font>)-[elkharr3@login1 ~]$ ml purge
(<font color="#06989A">yggdrasil</font>)-[elkharr3@login1 ~]$ ml GCC/12.3.0 CUDA/12.3.0 OpenMPI/4.1.5
(<font color="#06989A">yggdrasil</font>)-[elkharr3@login1 ~]$ ml

Currently Loaded Modules:
  1) GCCcore/12.3.0   3) binutils/2.40   5) CUDA/12.3.0      7) XZ/5.4.2         9) libpciaccess/0.17  11) OpenSSL/1.1      13) UCX/1.14.1        15) PMIx/4.2.4  17) OpenMPI/4.1.5
  2) zlib/1.2.13      4) GCC/12.3.0      6) numactl/2.0.16   8) libxml2/2.11.4  10) hwloc/2.9.1        12) libevent/2.1.12  14) libfabric/1.18.0  16) UCC/1.2.0
</pre>

## Compile

```bash
ml purge
ml GCC/12.3.0 CUDA/12.3.0 OpenMPI/4.1.5
make
```

## Run

```bash
sbatch run_slurm.sh
```

## Example Output

```bash
(yggdrasil)-[elkharr3@login1 reproducer-bug-yggdrasil]$ cat reproducer-34039128.out
World size : 4
Rank : 0
Hostname : gpu005.yggdrasil
Devices available : 3

World size : 4
Rank : 1
Hostname : gpu005.yggdrasil
Devices available : 3

World size : 4
Rank : 2
Hostname : gpu006.yggdrasil
Devices available : 1

World size : 4
Rank : 3
Hostname : gpu006.yggdrasil
Devices available : 1

```