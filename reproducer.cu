#include <cuda_runtime.h>
#include <mpi.h>

#include <iostream>

inline int getDeviceCount() {
    int count = -1;
    cudaGetDeviceCount(&count);
    return count;
};

inline std::string getHostname() {
    constexpr size_t kMaxHostnameLength = 256UL;

    std::string name;
    name.resize(kMaxHostnameLength);

    int name_size = -1;

    MPI_Get_processor_name(name.data(), &name_size);

    name.resize(name_size);
    return name;
};

int main(int argc, char* argv[]) {
    MPI_Init(&argc, &argv);

    int ws = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &ws);
    int rk = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rk);

    for (int i = 0; i < ws; i++) {
        if (i == rk) {
            std::cout << "World size : " << ws << std::endl;
            std::cout << "Rank : " << rk << std::endl;
            std::cout << "Hostname : " << getHostname() << std::endl;
            std::cout << "Devices available : " << getDeviceCount() << std::endl;
            std::cout << std::endl;
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
};